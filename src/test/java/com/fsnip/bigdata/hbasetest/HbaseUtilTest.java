package com.fsnip.bigdata.hbasetest;

import static org.junit.Assert.*;

import org.junit.Test;

public class HbaseUtilTest {

	@Test
	public void testCreateTable() {
		HbaseUtil.createTable("testtable1");
	}

	@Test
	public void testInsertData() {
		HbaseUtil.insertData("testtable1");
	}
	
	@Test
	public void testInsertData2() {
		HbaseUtil.insertData2("testtable1");
	}

	@Test
	public void testDropTable() {
		HbaseUtil.dropTable("testtable2");
	}

	@Test
	public void testDeleteRow() {
		HbaseUtil.deleteRow("testtable1", "112233bbbcccc");
	}

	@Test
	public void testDeleteByCondition() {
		fail("Not yet implemented");
	}

	@Test
	public void testQueryAll() {
		HbaseUtil.QueryAll("testtable1");
	}

	@Test
	public void testQueryByCondition1() {
		HbaseUtil.QueryByCondition1("testtable1");
	}

	@Test
	public void testQueryByCondition2() {
		HbaseUtil.QueryByCondition2("testtable1");
	}

	@Test
	public void testQueryByCondition3() {
		HbaseUtil.QueryByCondition3("testtable1");
	}
	
	@Test
	public void testQueryByRowkeyCondition1() {
		HbaseUtil.QueryByRowkeyCondition1("testtable1");
	}
	
	@Test
	public void testQueryByRowkeyCondition2() {
		HbaseUtil.QueryByRowkeyCondition2("testtable1");
	}
	
	@Test
	public void testQueryByRowkeyCondition21() {
//		HbaseUtil.QueryByRowkeyCondition21("testtable1", "^.{3}201609");
		HbaseUtil.QueryByRowkeyCondition21("testtable1", "^401");
	}
	
	@Test
	public void testQueryByRowkeyCondition3() {
		HbaseUtil.QueryByRowkeyCondition3("testtable1");
	} 
	
	@Test
	public void testQueryByRowkeyCondition4() {
		HbaseUtil.QueryByRowkeyCondition4("testtable1");
	}

}
